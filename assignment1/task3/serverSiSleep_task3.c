#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>
#include"socket.h"
#include<sys/types.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include "myFunction.h"
#include <time.h>
#include <unistd.h>

#define MAX_BUF_SIZE 1024 //dim massima per messaggio UDP
#define SERVER_PORT 1234//porta di comunicazione usata dal SERVER_PORT

int main(int argc, char* argv[]){
  struct sockaddr_in server_addr; //struttura contenente le info sull'indirizzo del SERVER
  server_addr.sin_family=AF_INET;
  server_addr.sin_port=htons(SERVER_PORT); //da byte host a byte di rete
  server_addr.sin_addr.s_addr=INADDR_ANY; //da stringa a possibile indirizzo Internet
  struct sockaddr_in client_addr; //struttura contenente le info sull'indirizzo del CLIENT
  int sfd; //server socket file descriptor
  int br; //risultato funzione bind()

  socklen_t cli_size=sizeof(client_addr); //ricava dai dati di server_addr la dimensione che avrà la socket
  char receivedData[MAX_BUF_SIZE]; //dati ricevuti
  char sendData[MAX_BUF_SIZE]; //dati inviati

  sfd=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); //crea il collegamento con l'indirizzo del server
  if(sfd<0){
    perror("socket"); //stampa errore se il collegamento non è andato a buon fine
    exit(EXIT_FAILURE);
  }

  br=bind(sfd,(struct sockaddr*)&server_addr,sizeof(server_addr));
  if(br<0){
    perror("bind"); //stampa errore se il collegamento non è andato a buon fine
    exit(EXIT_FAILURE);
  }

  //CLIENT ciclico, fino a quando non invia la stringa "exit" continua a mandare e ricevere messaggi,
  //se manda exit aspetta un goodye poi chiude

  for(;;){
    ssize_t byteRecv=recvfrom(sfd,receivedData,MAX_BUF_SIZE,0,(struct sockaddr*)&client_addr,&cli_size);
    char *ip_addr= inet_ntoa(client_addr.sin_addr);
    printf("indirizzo dal quale ricevo il messaggio: %s\n", ip_addr);
    printf("porta dalla quale ricevo il messaggio: %i\n",SERVER_PORT);
    if(byteRecv==-1){
      perror("recvfrom");
      exit(EXIT_FAILURE);
    }

    time_t ora;
    time (&ora);
    struct tm* orario;
    orario = localtime(&ora);
    printf("Dati ricevuti ore : %d:%d:%d ",orario->tm_hour,orario->tm_min,orario->tm_sec);
    printData(receivedData,byteRecv);

    if(strncmp(receivedData,"exit",byteRecv)==0){
      printf("Ho ricevuto il messaggio di chiusura della connessione\n");
      ssize_t byteSend=sendto(sfd,"Bye",byteRecv,0,(struct sockaddr*)&client_addr,sizeof(client_addr));
      if(byteSend!=byteRecv){
        perror("sendto");
        exit(EXIT_FAILURE);
      }
    }
    printf("Messaggio da Client:");
    printData(receivedData,byteRecv);
    sleep(3); //sleep per 10 secondi

    ssize_t byteSend=sendto(sfd,receivedData,byteRecv,0,(struct sockaddr*)&client_addr,sizeof(client_addr));
    if(byteSend!=byteRecv){
      perror("sendto");
      exit(EXIT_FAILURE);
    }
  }
  return 0;
}
