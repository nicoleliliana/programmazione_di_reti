#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>
#include"socket.h"
#include<sys/types.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include "myFunction.h"
#include <time.h>

#define MAX_BUF_SIZE 1024 //dim massima per messaggio UDP
#define SERVER_PORT 1234//porta di comunicazione usata dal SERVER_PORT
#define true 1
#define false 0

int main(int argc, char* argv[]){
  struct sockaddr_in server_addr; //struttura contenente le info sull'indirizzo del SERVER
  server_addr.sin_family=AF_INET;
  server_addr.sin_port=htons(SERVER_PORT); //da byte host a byte di rete
  server_addr.sin_addr.s_addr=inet_addr("127.0.0.1"); //da stringa a possibile indirizzo Internet
  struct sockaddr_in client_addr; //struttura contenente le info sull'indirizzo del CLIENT
  int sfd; //server socket file descriptor
  int br; //risultato funzione bind()
  int stop=false;

  socklen_t serv_size=sizeof(server_addr); //ricava dai dati di server_addr la dimensione che avrà la socket
  char receivedData[MAX_BUF_SIZE]; //dati ricevuti

  sfd=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); //crea il collegamento con l'indirizzo del server
  if(sfd<0){
    perror("socket"); //stampa errore se il collegamento non è andato a buon fine
    exit(EXIT_FAILURE);
  }

  //CLIENT ciclico, fino a quando non invia la stringa "exit" continua a mandare e ricevere messaggi,
  //se manda exit aspetta un goodye poi chiude

  while(true){
    //preparo messaggio da inviare
    time_t ora;
    time (&ora);
    struct tm* orario;
    orario = localtime(&ora);
    char sendData[]="Client2";
    printf("Stringa inviata al server : %s\n",sendData);
    printf("Stringa inviata ore : %d:%d:%d ",orario->tm_hour,orario->tm_min,orario->tm_sec);
    printf("\n");
    
    //invio messaggio
    size_t msgLen = countStrLen(sendData);
    ssize_t byteSend = sendto(sfd,sendData,msgLen,0,(struct sockaddr*)&server_addr,sizeof(server_addr));
    printf("messaggio inviato:%s\n",sendData);
    //ricezione dati
    ssize_t byteRecv = recvfrom(sfd, receivedData,MAX_BUF_SIZE,0,(struct sockaddr*)&server_addr, &serv_size);
    printf("Ricevuto dal server: ");
    printData(receivedData,byteRecv);
    printf("\n");
  }

}
